'use strict';
angular.module('rutasApp')
  .directive('navButtons', function () {
	return{
      restrict: 'E',
      templateUrl: 'views/nav-buttons.html',
    };
});