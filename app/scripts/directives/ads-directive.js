'use strict';

angular.module('rutasApp')

.directive('myAds', function() {
    return {
        restrict: 'A',
        templateUrl: 'views/ads.html',
        controller: function(){
            (adsbygoogle = window.adsbygoogle || []).push({});
        }
    };
})

.controller('adServerCtrl', ['$scope', function ($scope) {
	console.log("HOLA");
	
}]);