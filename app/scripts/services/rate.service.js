'use strict';

angular.module('rutasApp')

.service('RateService', ['$timeout', '$modal', '$log', function($timeout, $modal, $log){

  function rateApp(){
    openRateModal();
  }

  //to wait 5 minutes is the same that 350000 milliseconds
  //$timeout(rateApp, 350000);
  if(!localStorage.rateDone){
    var promise = $timeout(rateApp, 180000);
  }


  function openRateModal() {
    var modalInstance = $modal.open({
    templateUrl: 'views/rate-this-app.html',
    controller: 'ModalRatingCtrl',
    size: null,
  });

  modalInstance.result.then(function () {

    $timeout.cancel(promise);

  }, function () {
    $log.info('Modal dismissed at: ' + new Date());
    $timeout.cancel(promise);
  });
  }

}])

.controller('ModalRatingCtrl', ['$scope', '$modalInstance', '$modal', function ($scope, $modalInstance, $modal){

  Parse.initialize('Z6EWUM9VVRIe5JX3Iw2ZnLB1j2LQn9J0CU0DFXT4', 'cfs3GctSfGPGANhGS02dqS6JflMBl1WlebkmXcUw');
  $scope.rate = {};
	$scope.max = 5;

	$scope.hoveringOver = function(value) {
		$scope.overStar = value;
		$scope.percent = 100 * (value / $scope.max);
	};

$scope.saveRate = function () {
      var Rate = Parse.Object.extend('Rate');
      var rate = new Rate();
      rate.save({
      email: $scope.rate.email,
      comment: $scope.rate.comment,
      stars: $scope.rate.stars
    }, {
      success: function(object) {
        //$(".success").show();
        localStorage.rateDone = true;
        // var dialogInstance = $modal.open({
        //   template: '<div class="modal-body text-center">'+
        //             '<alert ng-model="message" type="{{alert}}" close="closeAlert()">'+
        //             '{{message}}</alert></div>',
        //   controller: 'MessageInstanceCtrl',
        //   size: null,
        //   resolve: {
        //   msg: function () {
        //     return 'Solicitud enviada correctamente';
        //   },
        //   alert: function () {
        //     return 'success';
        //   }
        // }
        // });
         /*var dialogInstance = $modal.open({
           template: '<div class="modal-body text-center">'+
                     '<alert ng-model="message" type="{{alert}}" close="closeAlert()">'+
                     '{{message}}</alert></div>',
           controller: 'MessageInstanceCtrl',
          size: null,
           resolve: {
           msg: function () {
             return 'Solicitud enviada correctamente';
           },
           alert: function () {
             return 'success';
           }
         }
         });*/
      },
      error: function(model, error) {
        //$(".error").show();
        /*var dialogInstance = $modal.open({
           template: '<div class="modal-body text-center">'+
                     '<alert ng-model="message" type="{{alert}}" close="closeAlert()">'+
                     '{{message}}</alert></div>',
           controller: 'MessageInstanceCtrl',
           size: null,
           resolve: {
           msg: function () {
           return 'Error: la solicitud no se pudo enviar';
           },
           alert: function () {
             return 'danger';
           }
         }
         });*/
      }
    });

    $scope.doRate();
  };

  $scope.doRate = function() {
    $modalInstance.close();
  };

  $scope.cancelRate = function () {
    $modalInstance.dismiss('cancel');
  };

}])

/*.controller('MessageInstanceCtrl', ['$scope', '$modalInstance', 'msg', 'alert', function ($scope, $modalInstance, msg, alert){
  $scope.message = msg;
  $scope.alert = alert;

  $scope.closeAlert = function() {
    $modalInstance.close();
  }
}]);*/
