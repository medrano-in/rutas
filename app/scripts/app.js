'use strict';

/**
 * @ngdoc overview
 * @name rutasApp
 * @description
 * # rutasApp
 *
 * Main module of the application.
 */
angular
  .module('rutasApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'uiGmapgoogle-maps'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .config(['uiGmapGoogleMapApiProvider', function (uiGmapGoogleMapApiProvider) {
      uiGmapGoogleMapApiProvider.configure({
          key: 'AIzaSyBpMZTqGZ9tPwNjBqBBwoBenCUuQEK9yNI',
          v: '3.17',
          libraries: 'weather,geometry,visualization'
     });
  }]);