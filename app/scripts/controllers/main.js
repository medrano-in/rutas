'use strict';

/**
 * @ngdoc function
 * @name rutasApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the rutasApp
 */
angular.module('rutasApp')

.controller('MainCtrl',
  ['$scope', '$rootScope', 'uiGmapGoogleMapApi', '$http', 'RateService', '$window', '$modal', '$log', '$q', 'uiGmapIsReady',
  function ($scope, $rootScope, uiGmapGoogleMapApi, $http, RateService, $window, $modal, $log, $q, uiGmapIsReady) {
    
    /* Define rutas disponibles */
    $scope.rutas = [
      {id: '01', name: 'Ruta 01'},
      {id: '02', name: 'Ruta 02'},
      {id: '03', name: 'Ruta 03'},
      {id: '04', name: 'Ruta 04'},
      {id: '05', name: 'Ruta 05'},
      {id: '06', name: 'Ruta 06'},
      {id: '07', name: 'Ruta 07'},
      {id: '08', name: 'Ruta 08'},
      {id: '09', name: 'Ruta 09'},
      {id: '10', name: 'Ruta 10'},
      {id: '11', name: 'Ruta 11'},
      {id: '12', name: 'Ruta 12'},
      {id: '14', name: 'Ruta 14'},
      {id: '15', name: 'Ruta 15'},
      {id: '16', name: 'Ruta 16'},
      {id: '18', name: 'Ruta 18'},
      {id: '19', name: 'Ruta 19'},
      {id: '20', name: 'Ruta 20'},
      {id: '21', name: 'Ruta 21'},
      {id: '23', name: 'Ruta 23'},
      {id: '23B', name: 'Ruta 23B'},
      {id: '24', name: 'Ruta 24'},
      {id: '25', name: 'Ruta 25'},
      {id: '27', name: 'Ruta 27'},
      {id: '28', name: 'Ruta 28'},
      {id: '29', name: 'Ruta 29'},
      {id: '30', name: 'Ruta 30'},
      {id: '31', name: 'Ruta 31'},
      {id: '33', name: 'Ruta 33'},
      {id: '34', name: 'Ruta 34'},
      {id: '35', name: 'Ruta 35'},
      {id: '36', name: 'Ruta 36'},
      {id: '37', name: 'Ruta 37'},
      {id: '38', name: 'Ruta 38'},
      {id: '39', name: 'Ruta 39'},
      {id: '40', name: 'Ruta 40'},
      {id: '41', name: 'Ruta 41'},
      {id: '42', name: 'Ruta 42'},
      {id: '43', name: 'Ruta 43'},
      {id: '44', name: 'Ruta 44'},
      {id: '45', name: 'Ruta 45'},
      {id: '46', name: 'Ruta 46'},
      {id: '47', name: 'Ruta 47'},
      {id: '48', name: 'Ruta 48'},
      {id: '50', name: 'Ruta 50'}
    ];
    /* END Define rutas disponibles */

    /* Define colores de rutas */
    $scope.colorRutas = [
      { ida: '#2ECCFA', regreso: '#FF8000'}, // Azul cielo - Naranja
      { ida: '#0040FF', regreso: '#DF0101'}, // Azul - Rojo
      { ida: '#6A0888', regreso: '#F0ED3B'}, // Purpura - Amarillo
      { ida: '#B404AE', regreso: '#51D15E'}, // Morado/rojo - Amarillo/verde
    ];
    /* END Define colores de rutas */

    /* Define centro y zoom del mapa */
    $scope.map = {
	    center: {
        latitude: 21.882886,
        longitude: -102.294878
	    },
	    zoom: 13
	  };
    /* END Define centro y zoom del mapa */

    /* Variables de mapa y rutas */
    var map;                      // Guarda el objeto de Mapa
    $scope.mapReady = false;      // Define si el mapa está cargado o no
    $scope.activatedRoutes = [];  // Arreglo para guardar las rutas presionadas
    var polylines = [];           // Arreglo para guardar los trazados de rutas
    var contador = -1;            // Contador para cambiar colores de trazados rutas
    var rutasOptimas = [];
    $scope.buspoints = [];
    $scope.places = [];//arreglo para guardar los lugares que se muestran en 'search'
    $scope.markerClick = [];
    /* END Variables de mapa y rutas */

    /* Guarda mapa en variable map. */
    uiGmapIsReady.promise(1).then(function(instances) {
      instances.forEach(function(inst) {
        map = inst.map;
      });
      $scope.mapReady = true;
    });
     /* END Guarda mapa en variable. */

    /* Opciones de ngAutocomplete (Search box de Google Maps) */
    $scope.resultAutocomplete = '';
    $scope.optionsAutocomplete = {
      country: 'MX'   // Limita búsqueda a México
    };
    $scope.detailsAutocomplete = '';
    /* END Opciones de ngAutocomplete (Search box de Google Maps) */

    /* Función para eventos de Google Analytics */
    $scope.analytics = function (label, type, name) {
      ga('send', 'event', label, type, name);
    };
    /* END Función para eventos de Google Analytics */

    /* Función para recargar página. */
    $scope.home = function(){
      $window.location.reload();
    };
    /* END Función para recargar página. */

    /* Función para regresar y guardar coordenadas de ubicación de usuario. */
    $scope.messageLocation = 'Mi ubicación';
    $scope.userLocation = function () {
      $scope.messageLocation = 'Localizando';
      navigator.geolocation.getCurrentPosition(function (pos) {
        var coords = $scope.getLocation(pos);

        /* Guardar coordendadas en Parse */
        Parse.initialize('Z6EWUM9VVRIe5JX3Iw2ZnLB1j2LQn9J0CU0DFXT4', 'cfs3GctSfGPGANhGS02dqS6JflMBl1WlebkmXcUw');
        var UserLocation = Parse.Object.extend('UserLocation');
        var saveCoords = new UserLocation();
        saveCoords.save({
          ubicacion: coords.A + ',' + coords.F
        }, {
          success: function(object) {
            //console.log('Cordenadas guardadas correctamente');
          },
          error: function(model, error) {
            console.log('Error al enviar cordenadas');
          }
        });
        /* END Guardar coordendadas en Parse */

      }, function (error) {
        swal({
          title: 'Lo sentimos, no es posible encontrar tú ubicación en este momento.',
          timer: 3000,
          showConfirmButton: false
        });
      });
    };
    /* END Función para regresar y guardar coordenadas de ubicación de usuario. */

    /* Función para localizar usuario en el mapa. */
    $scope.getLocation = function (pos) {
      var location = new google.maps.LatLng(pos.coords.latitude,pos.coords.longitude);
      $scope.$apply(function(){
        if ($scope.marker) {
          $scope.marker.setMap(null); 
        };
        var icon = 'images/marker2.png';
        $scope.marker = new google.maps.Marker({
          position: location,
          map: map,
          title: 'Mi Ubicación',
          icon: icon
        });
        map.setCenter(location);
        $scope.messageLocation = 'Mi ubicación';
      });
      return location;
    }
    /* END Función para localizar usuario en el mapa. */

    /* Función para limpiar coordenadas sin limpiar objetos del mapa. */
    $scope.clearCoords = function () {
      $scope.markerClick.position = null;
      $scope.routes = null;
      $scope.tempoPlaceName = null;
    };
    /* END Función para limpiar coordenadas sin limpiar objetos del mapa. */

    /* Función para limpiar marcadores de mapa */
    $scope.clearMarkers = function (map) {
      if ($scope.marker) {
        $scope.marker.setMap(map);  // Marcador de localización
      }
      if ($scope.markerPlace) {
        $scope.markerPlace.setMap(map);  // Marcador de lugar buscado
      }
      for (var i = 0; i < $scope.markerClick.length; i++) {
        $scope.markerClick[i].setMap(map);    // Marcador de click pulsados
      }
      $scope.markers = {};
      //$scope.buspoints = [];
    }
    /* END Función para limpiar marcadores de mapa */

    /* Función para limpiar rutas trazadas en el mapa */
    $scope.clearPolylines = function (map) {
      $scope.polylines = [];  // Limpia lineas trazadas
      contador = -1;          // Reiniciar colores de rutas
      $scope.markers = {};
    }
    /* END Función para limpiar rutas trazadas en el mapa */

    /* Función para limpiar todo del mapa */
    $scope.cleanMap = function(){
      angular.element('.activeRoute').removeClass('activeRoute'); // Limpia rutas seleccionadas de menu
      $scope.clearPolylines();  // Limpiar rutas de mapa
      $scope.clearMarkers();    // Limpiar marcadores de mapa
      /* Limpiar busqueda de rutas. */
      $scope.routes = null;
      $scope.tempoPlaceName = null;
      /* END Limpiar busqueda de rutas. */
    };
    /* END Función para limpiar todo del mapa */

    /* Función para resaltar rutas seleccionadas del menu */
    $scope.selectedRoute = function(id){
      angular.element('#'+id).addClass('activeRoute');
    };
    /* END Función para resaltar rutas seleccionadas del menu */

    /* Función para obtener una ruta de ida a regreso */
    $scope.getRoute = function(route){
      //console.log(route);
      $scope.markers = {};
      $scope.windowOptions = {
        visible: false
      };
      $scope.windowOptions2 = {
        visible: false
      };

      /* Muestra ruta de ida */
      $http.get('rutas/'+route+'_ida.json').success(function(data){
        uiGmapGoogleMapApi.then(function(maps){
          data.stroke.color =  $scope.colorRutas[contador].ida;

          data.icons[0].icon = {path: maps.SymbolPath.FORWARD_OPEN_ARROW};
          $scope.polylines.push(data);
          $scope.markers.push = {
            id: route,
            coords: {
              latitude: data.path[0].latitude,
              longitude: data.path[0].longitude
            }
          };

          //Para poner las imagenes en el mapa
          angular.forEach(data.buspoints, function(value, key) {
            var newBusPoint = {
              id: value.id,
              icon : {
                url: value.image.url,
                size: new google.maps.Size(32, 32),
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(0, 32)
              },
              coords: {
                latitude: value.latitude,
                longitude: value.longitude
              }
            }
            $scope.buspoints.push(newBusPoint);
          });  
        });
      }).error(function(data){
          //console.err(data);
      });
      /* END Muestra ruta de ida */

      /* Muestra ruta de regreso */
      $http.get('rutas/'+route+'_regreso.json').success(function(data){
        uiGmapGoogleMapApi.then(function(maps){
          data.stroke.color = $scope.colorRutas[contador].regreso;
          data.icons[0].icon = {path: maps.SymbolPath.FORWARD_OPEN_ARROW};
          $scope.polylines.push(data);
          $scope.markers.push = {
            id: route,
            coords: {
              latitude: data.path[0].latitude,
              longitude: data.path[0].longitude
            }
          };
        });
      }).error(function(data){
        //console.err(data);
      });
      /* END Muestra ruta de regreso */

      /* Contador para definir color a las rutas */
      contador++;
      if(contador > 3){
          contador= 0;
      }
      /* END Contador para definir color a las rutas */
    };
    /* END Función para obtener una ruta de ida a regreso */

    /* Función para obtener mas de dos rutas de ida a regreso */
    $scope.getAllRoutes = function (routes) {
      $scope.markers = {};
      $scope.windowOptions = {
        visible: false
      };
      $scope.windowOptions2 = {
        visible: false
      };
      var contadorIda = 0;
      var contadorReg = 0;
      for (var i=0; i<routes.length; i++) {
        /* Muestra rutas de ida */
        $http.get('rutas/'+routes[i]+'_ida.json').success(function(data){
          uiGmapGoogleMapApi.then(function(maps){
            data.stroke.color =  $scope.colorRutas[contadorIda].ida;
            data.icons[0].icon = {path: maps.SymbolPath.FORWARD_OPEN_ARROW};
            $scope.polylines.push(data);
            $scope.markers.push = {
              id: routes[i],
              coords: {
              latitude: data.path[0].latitude,
              longitude: data.path[0].longitude
            }
          };

          //Para poner las imagenes en el mapa
          angular.forEach(data.buspoints, function(value, key) {
          var newBusPoint = {
            id: value.id,
              icon : {
                url: value.image.url,
                size: new google.maps.Size(32, 32),
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(0, 32)
              },
              coords: {
                  latitude: value.latitude,
                  longitude: value.longitude
                }
              }
              $scope.buspoints.push(newBusPoint);
            });  
          });

          /* Contador para definir color a las rutas de ida */
          contadorIda++;
          if(contadorIda > 3){
            contadorIda = 0;
          }
          /* END Contador para definir color a las rutas de ida */
        }).error(function(data){
            //console.err(data);
        });
        /* END Muestra rutas de ida */

        /* Muestra rutas de regreso */
        $http.get('rutas/'+routes[i]+'_regreso.json').success(function(data){
          uiGmapGoogleMapApi.then(function(maps){
            data.stroke.color = $scope.colorRutas[contadorReg].regreso;
            data.icons[0].icon = {path: maps.SymbolPath.FORWARD_OPEN_ARROW};
            $scope.polylines.push(data);
            $scope.markers.push = {
              id: routes[i],
              coords: {
                latitude: data.path[0].latitude,
                longitude: data.path[0].longitude
              }
            };
          });
          /* Contador para definir color a las rutas de regreso */
          contadorReg++;
          if(contadorReg > 3){
              contadorReg = 0;
          }
          /* END Contador para definir color a las rutas de regreso */
        }).error(function(data){
          //console.err(data);
        });
        /* END Muestra rutas de regreso */
      }
    }
    /* END Función para obtener mas de dos rutas de ida a regreso */

    /* Función para buscar rutas con search box de google maps */
    $scope.getBestRoutesBySearchBox = function (destiny) {
      if (destiny) {
        var coordsPlace = destiny.geometry.location;
        var titlePlace = destiny.name;
        var iconPlace = {
          url: destiny.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(40, 40)
        };
        $scope.showCloseRoutesToLocation(coordsPlace);
        if ($scope.markerPlace) {
          $scope.markerPlace.setMap(null); 
        };
        $scope.markerPlace = new google.maps.Marker({
          position: coordsPlace,
          map: map,
          title: titlePlace,
          icon: iconPlace
        });
        map.setCenter(coordsPlace);
        $scope.clearCoords();
      } else {
        return;
      }
    }
    /* Función para buscar rutas con search box de google maps */

    /* Función para definir propiedades de rutas encontradas en la búsqueda (Recibe String) */
    /*$scope.findRoutes = function (routes, tempoPlaceName) {
      $scope.routes = routes.split(',');
      $scope.tempoPlaceName = tempoPlaceName;
      $scope.numRoutes = routes.split(',').length;
      if ($scope.numRoutes > 4) {
        $scope.numRoutes = 4;
      };
    }*/
    /* END Función para definir propiedades de rutas encontradas en la búsqueda (Recibe String) */

    /* Función para definir propiedades de rutas encontradas en la búsqueda (Recibe Array) */
    $scope.findCloseRoutes = function (routes, tempoPlaceName) {
      $scope.routes = routes;
      $scope.tempoPlaceName = tempoPlaceName;
      $scope.numRoutes = routes.length;
      $scope.buttonAllRoutes = true;
      /* Definir cantidad de columnas dependiendo al número de rutas */
      if ($scope.numRoutes == 0) {
        $scope.buttonAllRoutes = false;
      }else if ($scope.numRoutes <= 4) {
        $scope.numRoutes = 1;
      } else if ($scope.numRoutes > 4 && $scope.numRoutes <= 8) {
        $scope.numRoutes = 2;
        $scope.buttonAllRoutes = false;
      } else if ($scope.numRoutes > 8 && $scope.numRoutes <= 15) {
        $scope.numRoutes = 3;
        $scope.buttonAllRoutes = false;
      } else {
        $scope.numRoutes = 4;
        $scope.buttonAllRoutes = false;
      };
      /* END Definir cantidad de columnas dependiendo al número de rutas */
      /* Enviar información por broadcast (Los recibe $on('send-routes') */
      $rootScope.$broadcast('send-routes', {
        routes: $scope.routes,
        numRoutes: $scope.numRoutes,
        buttonAllRoutes: $scope.buttonAllRoutes,
        marker: $scope.marker,
        markerPlace: $scope.markerPlace
      });
      /* END Enviar información por broadcast (Los recibe $on('send-routes') */
    }
    /* END Función para definir propiedades de rutas encontradas en la búsqueda (Recibe Array) */

    /* Función mostrar rutas cercanas */
    $scope.showCloseRoutesToLocation = function(destiny){
      var routesToLoc
      if (!destiny) {
        // Busca rutas a partir de ubicacion de usuario
        routesToLoc = $scope.getCloseRoutesToLocation();
      } else {
        // Recibe un destino busca a partir de coordenadas recividas
        routesToLoc = $scope.getCloseRoutesToLocation(destiny);
      }
      routesToLoc.then(function(routes){
        $scope.findCloseRoutes(routes, 'Rutas encontradas');
      },function(error){
        console.log(error);
      });
    };
    /* END Función mostrar rutas cercanas */


/*****************************************/

//a partir de aqui son varias funciones para calcular la ruta mas cercana

$scope.getCloseRoutesToLocation = function(destiny){
    var deferred = $q.defer();
    var coords;
    var location;
    //navigator.geolocation.getCurrentPosition(function (pos) {
      if (!destiny) {
        navigator.geolocation.getCurrentPosition(function (pos) {
          coords = $scope.getLocation(pos);
          location = getCloseRoutesAnalyzer(coords);
          if(location)
            deferred.resolve(location);
          else
            deferred.reject('error');
        }, function (error) {
          console.log('Error');
        });
      } else {
        coords = destiny;
        location = getCloseRoutesAnalyzer(coords);
        if(location)
          deferred.resolve(location);
        else
          deferred.reject('error');
      }

    return deferred.promise;
};


function getCloseRoutesAnalyzer(coords){
  var deferred = $q.defer();
  //console.log(coords);
  $http.get('rutas/rutas_todas.json').success(function(data){
    var routePath;
    var aux;
    var inputMetros = [];
      angular.forEach(data, function(value, key){
          angular.forEach(value, function(val, k){
              
                routePath = new google.maps.Polyline({
                  path: val
                });
                //este if sirve para meter en el array solo las rutas que pasen a menos de 500 metros.
                if(Math.round(bdccGeoDistanceToPolyMtrs(routePath, coords)) <= 500){
                  //esta bandera sirve para no duplicar la ruta (ida y vuelta) y solo tener los numeros de ruta que pasan
                  if(k!=aux)
                    inputMetros.push(k);

                  aux = k;
                }
                //routePath.setMap($scope.map);
          });
      });
      
      if(inputMetros){
        deferred.resolve(inputMetros);
      }
      else{
        deferred.reject(console.log('error en analyzer'));
      }
      
  }).error(function(data){
      console.log('error');
  });
  return deferred.promise;

}

$scope.getBestRoutes = function(destiny, destiny2){
  //console.log(destiny);
    if (!destiny2) {
      var origin = $scope.getCloseRoutesToLocation();
    } else {
      var origin = getCloseRoutesAnalyzer(destiny2);
    }
    var destination = getCloseRoutesAnalyzer(destiny);
    var auxB = [];

    origin.then(function(routesO){
      destination.then(function(routesD){
        
        angular.forEach(routesO, function(value, key){
          angular.forEach(routesD, function(val, key){
            if(value==val){
              auxB.push(value);
            }
          });
        });

        var msgB;
        if(auxB.length>0){
          msgB = auxB;
          $scope.findCloseRoutes(auxB, 'Rutas desde tu ubicación al destino seleccionado');
        }
        else{
          msgB = '<h3>Lo sentimos, no hay rutas cercanas que te lleven a tu destino</h3>';
            if(msgB) {
              alertRoutes(msgB);
            }
            $scope.cleanMap();
        }
        
      },function(errorD){
        console.log('error destino ' + errorD);
      });
    }, function(errorO){
      console.log('error origen ' + errorO);
    });
}

function alertRoutes(msgB){
  //$scope.routes = null;
  //$scope.numRoutes = 0;
  $rootScope.$broadcast('send-routes', {
    routes: [],
    numRoutes: 0,
    buttonAllRoutes: $scope.buttonAllRoutes,
    marker: $scope.marker,
    markerPlace: $scope.markerPlace
  });
  /*swal({
    title: 'Sin resultados',
    html: msgB,
    showConfirmButton: false,
    timer: 3000
    });*/
}
    
// Code to find the distance in metres between a lat/lng point and a polyline of lat/lng points
// All in WGS84. Free for any use.
//
// Bill Chadwick 2007
// updated to Google Maps API v3, Lawrence Ross 2014
    
    // Construct a bdccGeo from its latitude and longitude in degrees
    function bdccGeo(lat, lon) 
    {
      var theta = (lon * Math.PI / 180.0);
      var rlat = bdccGeoGeocentricLatitude(lat * Math.PI / 180.0);
      var c = Math.cos(rlat); 
      this.x = c * Math.cos(theta);
      this.y = c * Math.sin(theta);
      this.z = Math.sin(rlat);    
    }
    bdccGeo.prototype = new bdccGeo();
    
    // internal helper functions =========================================
    
      // Convert from geographic to geocentric latitude (radians).
    function bdccGeoGeocentricLatitude(geographicLatitude) 
    {
      var flattening = 1.0 / 298.257223563;//WGS84
        var f = (1.0 - flattening) * (1.0 - flattening);
      return Math.atan((Math.tan(geographicLatitude) * f));
    }
    
    // Convert from geocentric to geographic latitude (radians)
    function bdccGeoGeographicLatitude (geocentricLatitude) 
    {
      var flattening = 1.0 / 298.257223563;//WGS84
        var f = (1.0 - flattening) * (1.0 - flattening);
      return Math.atan(Math.tan(geocentricLatitude) / f);
    }
    
     // Returns the two antipodal points of intersection of two great
     // circles defined by the arcs geo1 to geo2 and
     // geo3 to geo4. Returns a point as a Geo, use .antipode to get the other point
    function bdccGeoGetIntersection( geo1,  geo2,  geo3,  geo4) 
    {
      var geoCross1 = geo1.crossNormalize(geo2);
      var geoCross2 = geo3.crossNormalize(geo4);
      return geoCross1.crossNormalize(geoCross2);
    }
    
    //from Radians to Meters
    function bdccGeoRadiansToMeters(rad)
    {
      return rad * 6378137.0; // WGS84 Equatorial Radius in Meters
    }

    //from Meters to Radians
    function bdccGeoMetersToRadians(m)
    {
      return m / 6378137.0; // WGS84 Equatorial Radius in Meters
    }
    
    // properties =================================================
    

    bdccGeo.prototype.getLatitudeRadians = function() 
    {
      return (bdccGeoGeographicLatitude(Math.atan2(this.z,
        Math.sqrt((this.x * this.x) + (this.y * this.y)))));
    }

    bdccGeo.prototype.getLongitudeRadians = function() 
    {
      return (Math.atan2(this.y, this.x));
    }
    
    bdccGeo.prototype.getLatitude = function() 
    {
      return this.getLatitudeRadians()  * 180.0 / Math.PI;
    }

    bdccGeo.prototype.getLongitude = function() 
    {
      return this.getLongitudeRadians()  * 180.0 / Math.PI ;
    }

    // Methods =================================================

        //Maths
    bdccGeo.prototype.dot = function( b) 
    {
      return ((this.x * b.x) + (this.y * b.y) + (this.z * b.z));
    }

        //More Maths
    bdccGeo.prototype.crossLength = function( b) 
    {
      var x = (this.y * b.z) - (this.z * b.y);
      var y = (this.z * b.x) - (this.x * b.z);
      var z = (this.x * b.y) - (this.y * b.x);
      return Math.sqrt((x * x) + (y * y) + (z * z));
    }
    
    //More Maths
    bdccGeo.prototype.scale = function( s) 
    {
        var r = new bdccGeo(0,0);
        r.x = this.x * s;
        r.y = this.y * s;
        r.z = this.z * s;
      return r;
    }

        // More Maths
    bdccGeo.prototype.crossNormalize = function( b) 
    {
      var x = (this.y * b.z) - (this.z * b.y);
      var y = (this.z * b.x) - (this.x * b.z);
      var z = (this.x * b.y) - (this.y * b.x);
      var L = Math.sqrt((x * x) + (y * y) + (z * z));
      var r = new bdccGeo(0,0);
      r.x = x / L;
      r.y = y / L;
      r.z = z / L;
      return r;
    }
    
    // point on opposite side of the world to this point
    bdccGeo.prototype.antipode = function() 
    {
      return this.scale(-1.0);
    }


        //distance in radians from this point to point v2
    bdccGeo.prototype.distance = function( v2) 
    {
      return Math.atan2(v2.crossLength(this), v2.dot(this));
    }

    //returns in meters the minimum of the perpendicular distance of this point from the line segment geo1-geo2
    //and the distance from this point to the line segment ends in geo1 and geo2 
    bdccGeo.prototype.distanceToLineSegMtrs = function(geo1, geo2)
    {            
    
      //point on unit sphere above origin and normal to plane of geo1,geo2
      //could be either side of the plane
      var p2 = geo1.crossNormalize(geo2); 

      // intersection of GC normal to geo1/geo2 passing through p with GC geo1/geo2
      var ip = bdccGeoGetIntersection(geo1,geo2,this,p2); 

      //need to check that ip or its antipode is between p1 and p2
      var d = geo1.distance(geo2);
      var d1p = geo1.distance(ip);
      var d2p = geo2.distance(ip);
      //window.status = d + ', ' + d1p + ', ' + d2p;
      if ((d >= d1p) && (d >= d2p)) 
        return bdccGeoRadiansToMeters(this.distance(ip));
      else
      {
        ip = ip.antipode(); 
        d1p = geo1.distance(ip);
        d2p = geo2.distance(ip);
      }
      if ((d >= d1p) && (d >= d2p)) 
        return bdccGeoRadiansToMeters(this.distance(ip)); 
      else 
        return bdccGeoRadiansToMeters(Math.min(geo1.distance(this),geo2.distance(this))); 
    }

        // distance in meters from GLatLng point to GPolyline or GPolygon poly
        function bdccGeoDistanceToPolyMtrs(poly, point)
        {
            var d = 999999999;
            var i;
            var p = new bdccGeo(point.lat(),point.lng());
            for(i=0; i<(poly.getPath().getLength()-1); i++)
                 {
                    var p1 = poly.getPath().getAt(i);
                    var l1 = new bdccGeo(p1.lat(),p1.lng());
                    var p2 = poly.getPath().getAt(i+1);
                    var l2 = new bdccGeo(p2.lat(),p2.lng());
                    var dp = p.distanceToLineSegMtrs(l1,l2);
                    if(dp < d)
                        d = dp;    
                 }
             return d;
        }

        // get a new GLatLng distanceMeters away on the compass bearing azimuthDegrees
        // from the GLatLng point - accurate to better than 200m in 140km (20m in 14km) in the UK

        function bdccGeoPointAtRangeAndBearing (point, distanceMeters, azimuthDegrees) 
        {
             var latr = point.lat() * Math.PI / 180.0;
             var lonr = point.lng() * Math.PI / 180.0;
     
             var coslat = Math.cos(latr); 
             var sinlat = Math.sin(latr); 
             var az = azimuthDegrees* Math.PI / 180.0;
             var cosaz = Math.cos(az); 
             var sinaz = Math.sin(az); 
             var dr = distanceMeters / 6378137.0; // distance in radians using WGS84 Equatorial Radius
             var sind = Math.sin(dr); 
             var cosd = Math.cos(dr);
      
            return new google.maps.LatLng(Math.asin((sinlat * cosd) + (coslat * sind * cosaz)) * 180.0 / Math.PI,
            (Math.atan2((sind * sinaz), (coslat * cosd) - (sinlat * sind * cosaz)) + lonr) * 180.0 / Math.PI); 
        }


/*****************************************************************/

 $scope.onRouteClick = function() {
    $scope.windowOptions.visible = !$scope.windowOptions.visible;
  };

  $scope.closeRouteClick = function() {
      $scope.windowOptions.visible = false;
  };

  $scope.onBusClick = function(buspoint, stopCoords) {
    $scope.closeBusClick();
    $scope.windowOptions2.visible = !$scope.windowOptions2.visible;
    $scope.stopCoords = stopCoords;
    $scope.buspoint = buspoint;
    console.log('Bus opened');
    console.log(stopCoords);
    console.log(buspoint);
  };

  $scope.closeBusClick = function() {
    $scope.windowOptions2.visible = false;
    $scope.stopCoords = {
          latitude: 21.882886,
          longitude: -102.294878
      };
    $scope.buspoint = null;
    console.log('Bus closed');
  };

  uiGmapGoogleMapApi.then(function(maps){
    $scope.polylines = [{
      id: 7,
      path: [
        {latitude:21.971862,longitude:-102.345039}
      ],
      stroke: {
        color: '#CC0000',
        weight: 3
        },
      editable: false,
      draggable: false,
      geodesic: true,
      visible: true,
      icons: [
        {
        icon: {
        path: maps.SymbolPath.BACKWARD_OPEN_ARROW
        },
        offset: '25px',
        repeat: '50px'
        }
      ]
    }];
  });

  $scope.searchModal = function () {
    //console.log($scope.routes);
    var modalInstance = $modal.open({
        templateUrl: 'views/search.html',
        controller: 'SearchModalInstanceCtrl',
        scope: $scope,
        resolve: {
          marker: function() {
            return $scope.marker;
          },
          markerPlace: function() {
            return $scope.markerPlace;
          }
        },
        size: 'sm'
    });

    modalInstance.result.then(function (route) {
      if (route) {
        //console.log('Una ruta');  
        $scope.getRoute(route);
      } else {
        //console.log('Dos o mas rutas');
        $scope.getAllRoutes($scope.routes);
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  $scope.$on('send-routes', function(event, args) {
    $scope.routes = args.routes;
    $scope.numRoutes = args.numRoutes;
    $scope.buttonAllRoutes = args.buttonAllRoutes;
    $scope.marker = args.marker;
    $scope.markerPlace = args.markerPlace;
  });

  /* Función para cachar click's en el mapa */
  $scope.activeTwoPoints = false;
  $scope.clickMarker = function () {
    google.maps.event.addListener(map, 'click', function(event) {
        if (!$scope.activeTwoPoints) {
          $scope.cleanMap();
        }
        var coords = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
        var icon = 'images/marker1.png';
        var marker = new google.maps.Marker({
          position: coords,
          map: map,
          title: 'Nuevo destino',
          icon: icon
        });
        $scope.markerClick.push(marker);
        $scope.showOptionsClick();
    });
  }
  /* END Función para cachar click's en el mapa */

  /* Modal de opciones de búsqueda por puntos */
  $scope.showOptionsClick = function () {
    if (!$scope.activeTwoPoints) {
      var modalInstance = $modal.open({
        templateUrl: 'views/seachByPoint.html',
        controller: 'SearchByPointModalInstanceCtrl',
        scope: $scope,
        resolve: {
          clickMarker: function(){
            return $scope.markerClick;
          }
        },
        size: 'sm'
      });
    } else {
      var modalInstance = $modal.open({
        templateUrl: 'views/searchByTwoPoints.html',
        controller: 'SearchByPointModalInstanceCtrl',
        scope: $scope,
        resolve: {
          clickMarker: function(){
            return $scope.markerClick;
          }
        },
        size: 'sm'
      });
    }

    modalInstance.result.then(function (args) {
      if (args.index == 1) {
        $scope.routesByPoint(args.markerClick);
      } else if (args.index == 2) {
        $scope.routesByPointAndLocation(args.markerClick);
      } else if (args.index == 3) {
        $scope.activeTwoPoints = true;
        $scope.alertRoutesByPoint();
      } else {
        $scope.routesByPointToPoint(args.markerClick);
      }
    }, function () {
      $scope.activeTwoPoints = false;
      $scope.cleanMap();
      $log.info('Modal dismissed at: ' + new Date());
    });
  };
  /* END Modal de opciones de búsqueda por puntos */

  /* Rutas disponibles en punto pulsado */
  $scope.routesByPoint =function (markerClick) {
    var i = markerClick.length;
    var pos = markerClick[i-1].position;
    $scope.showCloseRoutesToLocation(pos);
    $scope.clearCoords();
    $scope.searchModal();
  }
  /* END Rutas disponibles en punto pulsado */

  /* Rutas disponibles de punto a tu ubicación*/
  $scope.routesByPointAndLocation = function (markerClick) {
    var i = markerClick.length;
    var pos = markerClick[i-1].position;
    $scope.getBestRoutes(pos);
    $scope.clearCoords();
    $scope.searchModal();
  }
  /* END Rutas disponibles de punto a tu ubicación*/

  /* Ver rutas disponibles de punto a punto */
  $scope.routesByPointToPoint = function (markerClick) {
    $scope.activeTwoPoints = true;
    var i = markerClick.length;
    var pos = markerClick[i-1].position;
    // Confirmar segundo punto de rutas optimas:
    if (markerClick[i-2]) {
      var pos2 = markerClick[i-2].position;
    }
    $scope.getBestRoutes(pos, pos2);
    //$scope.searchRoute();
    $scope.searchModal();
    $scope.activeTwoPoints = false;
    // END Confirmar segundo punto de rutas optimas.
  }
  /* END Ver rutas disponibles de punto a punto */

  $scope.alertRoutesByPoint = function () {
    swal({
      title: 'Selecciona un punto en el mapa...',
      timer: 1000,
      showConfirmButton: false
    });
  }

}]);