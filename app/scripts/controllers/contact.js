'use strict';
/**
 * @ngdoc function
 * @name promanApp.controller:CommentsCtrl
 * @description
 * # CommentsCtrl
 * Controller of the rutasApp
 */

angular.module('rutasApp')

.controller('CommentsCtrl', ['$scope', '$modal', '$log', function ($scope, $modal, $log){

  $scope.comment = [];

  Parse.initialize("Z6EWUM9VVRIe5JX3Iw2ZnLB1j2LQn9J0CU0DFXT4", "cfs3GctSfGPGANhGS02dqS6JflMBl1WlebkmXcUw");

  $scope.saveComment = function () {
    	var Contact = Parse.Object.extend("Contact");
      var contact = new Contact();
    	contact.save({
  		name: $scope.comment.Name,
  		email: $scope.comment.email,
  		phone: $scope.comment.phone,
  		category: $scope.comment.category,
  		comment: $scope.comment.comment
	  }, {
      success: function(object) {
        //$(".success").show();
        console.log("Enviado correctamente");
        var dialogInstance = $modal.open({
          template: '<div class="modal-body text-center">'+
                    '<alert ng-model="message" type="{{alert}}" close="closeAlert()">'+
                    '{{message}}</alert></div>',
          controller: 'MessageInstanceCtrl',
          size: null,
          resolve: {
          msg: function () {
            return 'Solicitud enviada correctamente';
          },
          alert: function () {
            return 'success';
          }
        }
        });
      },
      error: function(model, error) {
        //$(".error").show();
        console.log("Error al enviar");
        var dialogInstance = $modal.open({
          template: '<div class="modal-body text-center">'+
                    '<alert ng-model="message" type="{{alert}}" close="closeAlert()">'+
                    '{{message}}</alert></div>',
          controller: 'MessageInstanceCtrl',
          size: null,
          resolve: {
          msg: function () {
            return 'Error: la solicitud no se pudo enviar';
          },
          alert: function () {
            return 'danger';
          }
        }
        });
      }
    });
  }

  $scope.doComment = function () {
  	var modalInstance = $modal.open({
        templateUrl: 'views/contact.html',
        controller: 'ModalInstanceCtrl',
        size: null,
  	});

  	modalInstance.result.then(function (comment) {
	  }, function () {
	    $log.info('Modal dismissed at: ' + new Date());
	  });
	}
}])

.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', function ($scope, $modalInstance){
  $scope.ok = function () {
    $modalInstance.close($scope.comment);
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}])

.controller('MessageInstanceCtrl', ['$scope', '$modalInstance', 'msg', 'alert', function ($scope, $modalInstance, msg, alert){
  $scope.message = msg;
  $scope.alert = alert;

  $scope.closeAlert = function() {
    $modalInstance.close();
  }
}]);