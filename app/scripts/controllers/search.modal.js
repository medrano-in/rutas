'use strict';

/**
 * @ngdoc function
 * @name rutasApp.controller:SearchModalInstanceCtrl
 * @description
 * # SearchModalInstanceCtrl
 * Controller of the rutasApp
 */

angular.module('rutasApp')

.controller('SearchModalInstanceCtrl',
  ['$scope', '$rootScope', '$modalInstance', 'marker', 'markerPlace',
  function ($scope, $rootScope, $modalInstance, marker, markerPlace){

  $scope.ok = function (route) {
    $modalInstance.close(route);
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.cleanSearch = function () {
    $rootScope.$broadcast('send-routes', {
      routes: null,
      numRoutes: 1,
      buttonAllRoutes: false,
      marker: $scope.marker,
      markerPlace: $scope.markerPlace
    });
    $scope.cleanMap();
  };

}])

/**
 * @ngdoc function
 * @name rutasApp.controller:SearchByPointModalInstanceCtrl
 * @description
 * # SearchByPointModalInstanceCtrl
 * Controller of the rutasApp
 */

.controller('SearchByPointModalInstanceCtrl',
  ['$scope', '$modalInstance', 'clickMarker',
  function ($scope, $modalInstance, clickMarker){
  $scope.markerClick = clickMarker;
  $scope.okPoint = function (index) {
    $modalInstance.close({markerClick: $scope.markerClick, index: index});
  };
  $scope.cancelPoint = function () {
    $modalInstance.dismiss('cancel');
  };

}]);
