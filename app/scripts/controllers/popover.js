'use strict';
/**
 * @ngdoc function
 * @name promanApp.controller:UserLocationCtrl
 * @description
 * # UserLocationCtrl
 * Controller of the rutasApp
 */

angular.module('rutasApp')

.controller('PopoverCtrl', ['$scope', function ($scope){
  $scope.dynamicPopover = {
    content: 'Hello, World!',
    templateUrl: 'myPopoverTemplate.html',
    title: 'Title'
  };
}]);